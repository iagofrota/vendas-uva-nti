create database vendas_nti;

create table vendas_nti.public.migrations
(
    id        serial       not null
        constraint migrations_pkey
            primary key,
    migration varchar(255) not null,
    batch     integer      not null
);

create table vendas_nti.public.users
(
    id                bigserial    not null
        constraint users_pkey
            primary key,
    name              varchar(255) not null,
    email             varchar(255) not null
        constraint users_email_unique
            unique,
    email_verified_at timestamp(0),
    password          varchar(255) not null,
    remember_token    varchar(100),
    created_at        timestamp(0),
    updated_at        timestamp(0)
);

create table vendas_nti.public.password_resets
(
    email      varchar(255) not null,
    token      varchar(255) not null,
    created_at timestamp(0)
);

create index password_resets_email_index
    on vendas_nti.public.password_resets (email);

create table vendas_nti.public.failed_jobs
(
    id         bigserial                              not null
        constraint failed_jobs_pkey
            primary key,
    connection text                                   not null,
    queue      text                                   not null,
    payload    text                                   not null,
    exception  text                                   not null,
    failed_at  timestamp(0) default CURRENT_TIMESTAMP not null
);

create table vendas_nti.public.files
(
    id         bigserial    not null
        constraint files_pkey
            primary key,
    real_name  varchar(255) not null,
    fake_name  varchar(255) not null,
    extension  varchar(10)  not null,
    size       bigint       not null,
    created_at timestamp(0),
    updated_at timestamp(0),
    deleted_at timestamp(0)
);

create table vendas_nti.public.categories
(
    id         bigserial    not null
        constraint categories_pkey
            primary key,
    name       varchar(255) not null,
    created_at timestamp(0),
    updated_at timestamp(0),
    deleted_at timestamp(0)
);

create table vendas_nti.public.products
(
    id             bigserial        not null
        constraint products_pkey
            primary key,
    name           varchar(255)     not null,
    description    text             not null,
    sale_price     double precision not null,
    purchase_price double precision not null,
    category_id    bigint           not null
        constraint products_category_id_foreign
            references vendas_nti.public.categories,
    file_id        bigint           not null
        constraint products_file_id_foreign
            references vendas_nti.public.files,
    stock          bigint           not null,
    created_at     timestamp(0),
    updated_at     timestamp(0),
    deleted_at     timestamp(0)
);

create table vendas_nti.public.clients
(
    id         bigserial    not null
        constraint clients_pkey
            primary key,
    name       varchar(255) not null,
    cpf        varchar(11)  not null,
    telephone  varchar(11)  not null,
    email      varchar(255) not null,
    created_at timestamp(0),
    updated_at timestamp(0),
    deleted_at timestamp(0)
);

create table vendas_nti.public.sales
(
    id                     bigserial        not null
        constraint sales_pkey
            primary key,
    client_id              bigint           not null
        constraint sales_client_id_foreign
            references vendas_nti.public.clients,
    discount               double precision not null,
    total_without_discount double precision not null,
    total_with_discount    double precision not null,
    public_id              varchar(255)     not null
        constraint sales_public_id_unique
            unique,
    created_at             timestamp(0),
    updated_at             timestamp(0),
    deleted_at             timestamp(0)
);

create table vendas_nti.public.product_sale
(
    id         bigserial not null
        constraint product_sale_pkey
            primary key,
    sale_id    bigint    not null
        constraint product_sale_sale_id_foreign
            references vendas_nti.public.sales,
    product_id bigint    not null
        constraint product_sale_product_id_foreign
            references vendas_nti.public.products,
    amount     bigint    not null,
    created_at timestamp(0),
    updated_at timestamp(0),
    deleted_at timestamp(0)
);

create table vendas_nti.public.jobs
(
    id           bigserial    not null
        constraint jobs_pkey
            primary key,
    queue        varchar(255) not null,
    payload      text         not null,
    attempts     smallint     not null,
    reserved_at  integer,
    available_at integer      not null,
    created_at   integer      not null
);

create index jobs_queue_index
    on vendas_nti.public.jobs (queue);

