<?php

namespace App;

use Hamcrest\Util;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function file()
    {
        return $this->belongsTo(Files::class);
    }

    public function sales()
    {
        return $this->belongsToMany(Sale::class)->withPivot(['amount']);
    }

    public function getSalePriceAttribute($value)
    {
        return Utils::currencyToDisplay($value);
    }

    public function setSalePriceAttribute($value)
    {
        $this->attributes['sale_price'] = Utils::currencyToBase($value);
    }

    public function getPurchasePriceAttribute($value)
    {
        return Utils::currencyToDisplay($value);
    }

    public function setPurchasePriceAttribute($value)
    {
        $this->attributes['purchase_price'] = Utils::currencyToBase($value);
    }
}
