<?php

namespace App\Http\Controllers;

use App\Category;
use App\Files;
use App\Product;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Utils;
use Illuminate\Support\Facades\Storage;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('category')->where(function ($query) {
            if (!empty($_GET['name'])) {
                $query->whereRaw("products.name ilike '%{$_GET['name']}%'");
            }

            if (!empty($_GET['category_id'])) {
                $query->where('products.category_id', $_GET['category_id']);
            }
        })->where('products.deleted_at', null)->paginate(10);

        return view('products.index')->with('products', $products)->with('categories', Category::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = new Category();

        return view('products.create')->with('categories', $categories->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $fake_name = Storage::putFile('', new File($request->file('file')));

            $file = new Files();

            $file->real_name = $request->file('file')->getClientOriginalName();
            $file->fake_name = $fake_name;
            $file->size = $request->file('file')->getSize();
            $file->extension = $request->file('file')->extension();

            $file->save();

            $product = new Product();

            $product->name = $request->get('name');
            $product->description = $request->get('description');
            $product->sale_price = $request->get('sale_price');
            $product->purchase_price = $request->get('purchase_price');
            $product->category_id = $request->get('category_id');
            $product->stock = $request->get('stock');
            $product->file_id = $file->id;

            $product->save();

            DB::commit();

            return redirect()
                ->route('products.index')
                ->with('success', 'Produto adicionado com sucesso');
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()
                ->route('products.index')
                ->with('error', 'Hpuve um erro ao tentar alterar o produto');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        $product->load(['category', 'file']);

        return view('products.edit')
            ->with('categories', $categories)
            ->with('file', Storage::get($product->file->fake_name))
            ->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        try {
            DB::beginTransaction();

            if ($request->hasFile('file')) {
                $product->load('file');

                Storage::delete($product->file->fake_name);
                Files::where('id', $product->file->id)->delete();

                $file = new Files();
                $fake_name = Storage::putFile('', new File($request->file('file')));

                $file->real_name = $request->file('file')->getClientOriginalName();
                $file->fake_name = $fake_name;
                $file->size = $request->file('file')->getSize();
                $file->extension = $request->file('file')->getExtension();

                $file->save();

                $product->file_id = $file->id;
            }

            $product->name = $request->get('name');
            $product->description = $request->get('description');
            $product->sale_price = $request->get('sale_price');
            $product->purchase_price = $request->get('purchase_price');
            $product->category_id = $request->get('category_id');
            $product->stock = $request->get('stock');

            $product->save();

            DB::commit();

            return redirect()
                ->route('products.index')
                ->with('success', 'Produto alterado com sucesso');
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()
                ->route('products.index')
                ->with('error', 'Houve um erro ao tentar alterar o produto');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $is_exists = $product->whereHas('sales', function ($query) use ($product) {
            $query->where('product_sale.product_id', $product->id);
            $query->where('product_sale.deleted_at', null);
        })->where('products.deleted_at', null)->exists();

        if (!$is_exists) {
            if ($product->delete()) {
                return redirect()
                    ->route('products.index')
                    ->with('success', 'Produto excluído com sucesso');
            }

            return redirect()
                ->route('products.index')
                ->with('error', 'Houve um erro ao tentar excluir o produto');

        }

        return redirect()
            ->route('products.index')
            ->with('error', 'O produto não pode ser excluído pois está sendo usado');
    }
}
