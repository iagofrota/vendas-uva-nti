<?php

namespace App\Http\Controllers;

use App\Client;
use App\Jobs\SendEmail;
use App\Mail\SendMailUser;
use App\Product;
use App\Sale;
use App\Utils;
use Carbon\Carbon;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Mail;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sale::with('client')->where(function ($query) {
            if (!empty($_GET['name'])) {
                $query->whereHas('client', function ($query) {
                    $query->whereRaw("clients.name ilike '%{$_GET['name']}%'");
                });
            }
        })->paginate(10);

        return view('sales.index')->with('sales', $sales);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sales.create')
            ->with('products', Product::with('file')->orderBy('name')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            // TODO: Salvar o cliente
            $client = Client::where('cpf', Utils::onlyNumbers($request->get('cpf')))->first();

            if (empty($client)) {
                $client = new Client();

                $client->name = $request->get('name');
                $client->cpf = Utils::onlyNumbers($request->get('cpf'));
                $client->telephone = Utils::onlyNumbers($request->get('telephone'));
                $client->email = $request->get('email');

                $client->save();
            }

            // TODO: Salvar a venda
            $sale = new Sale();
            $products = collect(json_decode($request->get('products'), FALSE));

            $sale = $sale->registerSale((object)[
                'client_id' => $client->id,
                'discount' => Utils::currencyToBase($request->get('discount')),
                'products' => $products,
            ]);

            // TODO: Salvar product_sale
            foreach ($products as $product) {
                $now = Carbon::now();

                $array_product_id[$product->id] = [
                    'amount' => $product->quantity,
                    'created_at' => $now,
                    'updated_at' => $now,
                ];

                $productTemp = Product::find($product->id);

                if ($product->quantity > $productTemp->stock) {
                    throw new \Exception('Quantidade maior que o estoque do Produto ' . $productTemp->name);
                }

                $productTemp->stock = intval($productTemp->stock) - intval($product->quantity);
                $productTemp->save();
            }

            $sale->products()->sync($array_product_id);

            DB::commit();

            SendEmail::dispatch($sale);

            return redirect()
                ->route('sales.index')
                ->with('success', 'Venda registrada com sucesso');
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()
                ->route('sales.create', $sale)
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Sale $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Sale $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        $sale->load(['products.file', 'client']);
        $products = Product::with('file')->orderBy('name')->get();

        return view('sales.edit')->with('sale', $sale)->with('products', $products);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Sale $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        try {
            DB::beginTransaction();

            // TODO: Salvar o cliente
            $client = Client::where('cpf', Utils::onlyNumbers($request->get('cpf')))->first();

            if (empty($client)) {
                $client = new Client();

                $client->name = $request->get('name');
                $client->cpf = Utils::onlyNumbers($request->get('cpf'));
                $client->telephone = Utils::onlyNumbers($request->get('telephone'));
                $client->email = $request->get('email');

                $client->save();
            }

            // TODO: Salvar a venda
            $products = collect(json_decode($request->get('products'), FALSE));

            $sale = $sale->updateSale($sale->id, (object)[
                'client_id' => $client->id,
                'discount' => Utils::currencyToBase($request->get('discount')),
                'products' => $products,
            ]);

            $currentProduct = Product::whereHas('sales', function ($query) use ($sale) {
                $query->where('sales.id', $sale->id);
            })->get('id');

            // TODO: Salvar product_sale
            $array_product_id = [];

            foreach ($products as $product) {
                $now = Carbon::now();

                // TODO: Validação ao tentar adicionar um novo produto
                if ($currentProduct->firstWhere('id', $product->id)) {
                    continue;
                }

                $array_product_id[$product->id] = [
                    'amount' => $product->quantity,
                    'created_at' => $now,
                    'updated_at' => $now,
                ];

                $productTemp = Product::find($product->id);

                if ($product->quantity > $productTemp->stock) {
                    throw new \Exception('Quantidade maior que o estoque do Produto ' . $productTemp->name);
                }

                $productTemp->stock = intval($productTemp->stock) - intval($product->quantity);
                $productTemp->save();
            }

            if (!empty($array_product_id)) {
                $sale->products()->sync($array_product_id);
            }

            DB::commit();

            return redirect()
                ->route('sales.index')
                ->with('success', 'Venda registrada com sucesso');
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()
                ->route('sales.edit', $sale)
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Sale $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        try {
            DB::beginTransaction();
            // TODO: Retornar os valores ao estoque
            $products = Sale::with('products')->where('sales.id', $sale->id)->first();

            foreach ($products->products as $product) {
                $productTemp = Product::find($product->pivot->product_id);

                $productTemp->stock = intval($productTemp->stock) + intval($product->pivot->amount);

                $productTemp->save();
            }

            // TODO: Excluir a venda
            $sale->delete();

            DB::commit();

            return redirect()
                ->route('sales.index')
                ->with('success', 'Venda excluída com sucesso');
        } catch (\Exception $e) {
            DB::rollBack();

            dd($e, $products);

            return redirect()
                ->route('sales.index')
                ->with('error', 'Houve um erro ao tentar excluir a venda');
        }
    }
}
