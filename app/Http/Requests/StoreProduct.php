<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|string',
            'description' => 'required|max:255|string',
            'telephone' => 'max:16|string',
            'category_id' => 'required|integer',
            'stock' => 'required|numeric',
            'file' => 'required|mimes:jpeg,png,jpg',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome do produto é obrigatório',
            'name.max' => 'O nome do produto é maior que 255 caracteres',
            'name.string' => 'O nome do produto é inválido',
            'description.required' => 'A descrição do produto é obrigatório',
            'description.max' => 'A descrição do produto é maior que 255 caracteres',
            'description.string' => 'A descrição do produto é inválida',
            'category_id.required' => 'A categoria é obrigatória',
            'category_id.numeric' => 'A categoria é inválida',
            'stock.required' => 'O estoque é obrigratório',
            'stock.numeric' => 'O estoque é inválido',
            'file.required' => 'A imagem do produto é obrigatória',
            'file.mimes' => 'A imagem do produto é inválida',
        ];
    }
}
