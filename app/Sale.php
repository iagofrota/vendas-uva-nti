<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Sale extends Model
{
    use SoftDeletes;

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot(['amount']);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function getTotalWithoutDiscountAttribute($value)
    {
        return Utils::currencyToDisplay($value);
    }

    public function getTotalWithDiscountAttribute($value)
    {
        return Utils::currencyToDisplay($value);
    }

    public function getCreatedAtAttribute($value)
    {
        return Utils::dateFormat($value, true);
    }

    public function registerSale($data)
    {
        $sale = new Sale();

        $sale->client_id = $data->client_id;
        $sale->discount = Utils::currencyToBase($data->discount);

        $total_without_discount = $data->products->reduce(function ($carry, $item) {
           return  $carry + $item->total;
        });

        $total_with_discount = $total_without_discount - ($total_without_discount * (floatval($sale->discount)/100));

        $sale->total_without_discount = $total_without_discount;
        $sale->total_with_discount = $total_with_discount;

        $sale->public_id = Str::random();

        $sale->save();

        return $sale;
    }

    public function updateSale($id, $data)
    {
        $sale = Sale::find($id);

        $sale->discount = Utils::currencyToBase($data->discount);

        $total_without_discount = $data->products->reduce(function ($carry, $item) {
            return  $carry + $item->total;
        });

        $total_with_discount = $total_without_discount - ($total_without_discount * (floatval($sale->discount)/100));

        $sale->total_without_discount = $total_without_discount;
        $sale->total_with_discount = $total_with_discount;

        $sale->save();

        return $sale;
    }
}
