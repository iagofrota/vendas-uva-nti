<?php


namespace App;


class Utils
{
    public static function currencyToBase($string)
    {
        return (double)str_replace(',', '.', str_replace('.', '', $string));
    }

    public static function currencyToDisplay($string)
    {
        return number_format($string, 2, ',', '.');
    }

    public static function onlyNumbers($string)
    {
        return preg_replace('/[^0-9]/', '', $string);
    }

    public static function cpfCnpjFormat($valor)
    {
        if (strlen($valor) > 11)
            return self::cnpjFormat($valor);
        else
            return self::cpfFormat($valor);
    }

    public static function isValidCpf($cpf)
    {
        $acpf = preg_replace('/[^0-9]/', '', (string)$cpf);
        if (strlen($acpf) == 11) {
            if ($acpf == str_repeat(substr($acpf, 0, 1), 11)) {
                return false;
            } else {
                for ($a = 9; $a < 11; $a++) {
                    for ($b = 0, $c = 0; $c < $a; $c++) {
                        $b += $acpf[$c] * (($a + 1) - $c);
                    };
                    $b = ((10 * $b) % 11) % 10;
                    if ($acpf[$c] != $b): return false;
                    endif;
                };
                return true;
            }
        } else {
            return false;
        }
    }

    public static function dateFormat($datetime, $withTime = false)
    {
        if (empty($datetime)) {
            return '';
        }
        list($date, $time) = explode(' ', $datetime.' ');
        list($time, $milliseconds) = explode('.', $time.'.');
        list($year, $month, $day) = explode('-', $date);

        return "{$day}/{$month}/{$year}".($withTime ? " às {$time}" : '');
    }
}
