<?php

namespace App\Observers;

use App\Jobs\SendEmail;
use App\Sale;

class SaleObserver
{
    /**
     * Handle the sale "created" event.
     *
     * @param  \App\Sale  $sale
     * @return void
     */
    public function created(Sale $sale)
    {
        SendEmail::dispatch($sale);
    }
}
