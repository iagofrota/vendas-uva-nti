# Vendas UVA - NTI

Projeto para a Seleção da Vaga de Analista PHP para o NTI da UVA.

## Autor
[Iago Olímpio Frota](https://iagofrota.com.br/)

## Tecnologias utilizadas
1. PHP 7.2
2. Laravel Framework 6.18.40
3. Postgres 12
6. JQuery 3.2
7. Bootstrap 4.5.2

## Configurações para rodar o projeto

1. Clonar o projeto `git clone https://gitlab.com/iagofrota/vendas-uva-nti.git`
2. Entre na pasta do projeto clonado e execute os seguintes comandos
    1. `composer install`
    2. `php artisan key:generate`
    3. `npm install`
    4. `npm run dev`
3. Renomeie o arquivo [.env.example](https://gitlab.com/iagofrota/vendas-uva-nti/-/blob/master/.env.example) para .env
4. No arquivo `.env`, coloque as credencias do seu banco de dados e do seu serviço de envio de e-mail. No meu caso, utilizei o serviço do [Mailtrap](https://mailtrap.io/) para testar o envio de email
5. Execute as _migrations_ e as _seeds_ com o comando `php artisan db:seed`

Para acessar logar no sistema, utilize as seguintes credencias

**e-mail:** vendas@nti.com


**senha:** secret


## DER do banco de dados
![der_banco](docs/der_banco.png)

## DDL do banco de dados
[DDL do banco de dados](https://gitlab.com/iagofrota/vendas-uva-nti/-/blob/ac80cd600e691b01266c166a36a51d0c8641244e/docs/script_banco.sql)

## Vídeo 1
Demonstração do sistema em execução (cadastro de produto e venda)

[https://1drv.ms/v/s!AvLQQqQRzDfRn-YAE-wZyFy4TqB_Yw?e=jd7huV](https://1drv.ms/v/s!AvLQQqQRzDfRn-YAE-wZyFy4TqB_Yw?e=jd7huV)

## Vídeo 2
Mostrar banco de dados populado após operações do vídeo anterior com acesso via PGAdmin


[https://1drv.ms/v/s!AvLQQqQRzDfRn-V-iKxW7uHpJqkVmg?e=lb57cp](https://1drv.ms/v/s!AvLQQqQRzDfRn-V-iKxW7uHpJqkVmg?e=lb57cp)

## Vídeo 3
Realizar um debug do código da aplicação em alguma parte de sua escolha

[https://1drv.ms/v/s!AvLQQqQRzDfRn-V_fmTqiix6-1GmsA?e=OXAIcB](https://1drv.ms/v/s!AvLQQqQRzDfRn-V_fmTqiix6-1GmsA?e=OXAIcB)




