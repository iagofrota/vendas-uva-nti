$(document).ready(function () {
    const modalRemoveRecordModel = $('#modal-confirmation-destroy')

    $('.remove-record').click(function () {
        let body = $('body').find('.remove-record-model')
        let $this = $(this)
        let id = $this.data('id')
        let url = $this.data('url')

        modalRemoveRecordModel.attr('action', url)
        body.append(`<input name="_method" type="hidden" value="DELETE">`)
        body.append(`<input name="id" type="hidden" value="${id}">`)
    });

    $('.remove-data-from-delete-form').click(function () {
        let body = $('body').find('.remove-record-model')

        body.find("input").remove()
    });
});