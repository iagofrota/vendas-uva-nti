$(function () {
    const btnAddProduct = $('#btn_add_product')
    const discount = $('#discount')

    if ($('#products').val() != '' && $('#products').val() != undefined) {
        const products = JSON.parse($('#products').val())

        console.log(products)

        productsSelected = products.map(function (elem) {
            return {
                id: elem.id,
                name: elem.name,
                quantity: parseInt(elem.pivot.amount),
                sale_price: parseFloat(elem.sale_price),
                total: parseFloat(elem.sale_price) * parseInt(elem.pivot.amount),
                file: elem.file.fake_name,
            }
        })

        reloadTableProduct()
    }

    btnAddProduct.click(function (event) {
        if (isProductExists(selectProduct.val())) {
            return
        }

        addProducts()
        clearFields()
    })

    discount.change(function () {
        refreshTotal()
    })
})

var products = $('#json_products').val() != undefined ? JSON.parse($('#json_products').val()) : []
var productsSelected = []
var productPrepareEdit = null
var selectProduct = $('#product_id')
var quantity = $('#quantity')
var jsonProducts = $('#json_products')
var totalWithDiscount = $('#total_with_discount')
var total = $('#total')
var discount = $('#discount')

function addProducts() {
    const productTemp = products.find((product) => product.id == selectProduct.val())

    productsSelected.push({
        id: productTemp.id,
        name: productTemp.name,
        quantity: parseInt(quantity.val()),
        sale_price: parseFloat(productTemp.sale_price),
        total: parseFloat(productTemp.sale_price) * parseInt(quantity.val()),
        file: productTemp.file.fake_name,
    })

    console.log(productsSelected)
    reloadTableProduct()
    refreshTotal()
}

function prepareEditProduct(product_id) {
    productPrepareEdit = productsSelected.find((product) => product.id == product_id)
    selectProduct.val(productPrepareEdit.id)
    quantity.val(productPrepareEdit.quatity)
}

function updateProduct(product_id) {
    const product = products.find((product) => product.id == product_id)

    product.id = productPrepareEdit.id
    product.name = productPrepareEdit.name
    product.quantity = parseInt(quantity.val())
    sale_price: parseFloat(product.sale_price)
    product.total = parseFloat(productPrepareEdit.sale_price) * parseInt(quantity.val())

    refreshTotal()

    productPrepareEdit = null
}

function destroyProduct(product_id) {

}

function reloadTableProduct() {
    const tableProduct = $('.table_product')
    const tbodyProduct = $('.tbody_product')

    tbodyProduct.empty()

    $.each(productsSelected, function (index, value) {
        let novaLinhaEl = $("<tr>");
        let linhas = `
                    <td><img src="/storage/${value.file}" class="h-25" /></td>
                    <td>${value.name}</td>
                    <td>${value.quantity}</td>
                    <td>${value.sale_price.toLocaleString("pt-BR", {
                            minimumFractionDigits: 2,
                            style: 'currency',
                            currency: 'BRL'
                        })}
                    </td>
                    <td>${value.total.toLocaleString("pt-BR", {
                            minimumFractionDigits: 2,
                            style: 'currency',
                            currency: 'BRL'
                        })}
                    </td>
                    `

        novaLinhaEl.append(linhas)
        tableProduct.append(novaLinhaEl)
    })

    $('#products').val(JSON.stringify(productsSelected))
}

function clearFields() {
    selectProduct.val('')
    quantity.val('')
}

function refreshTotal() {
    const totalWithoutDiscount = productsSelected.reduce(function (total, elem) {
        return total + elem.total
    }, 0)

    total.val(totalWithoutDiscount.toLocaleString("pt-BR", {
        minimumFractionDigits: 2,
        style: 'currency',
        currency: 'BRL'
    }))

    if (parseFloat(discount.val()) > 0) {
        const tempTotal = totalWithoutDiscount - (totalWithoutDiscount * (parseFloat(discount.val()) / 100))
        totalWithDiscount.val(tempTotal.toLocaleString("pt-BR", {
            minimumFractionDigits: 2,
            style: 'currency',
            currency: 'BRL'
        }))
    }
}

function isProductExists(product_id) {
    return productsSelected.find((product) => product_id == product.id)
}
