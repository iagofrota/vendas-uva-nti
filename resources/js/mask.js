$(document).ready(function () {
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.integer').mask('0000000', {reverse: true});
    $('.cep').mask('00.000-000');
    $('.phone').mask('(00) 0000-0000');
    $('.mobile-phone').mask('(00) 0.0000-0000');
    $('.money').mask('000.000,00', {reverse: true});
    $('.birthday').mask('00/00/0000', {reverse: true});
});
