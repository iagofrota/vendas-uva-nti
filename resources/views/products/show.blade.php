@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="main main-raised" style="margin-top: 100px">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 20px; display: flex; justify-content: flex-end;">
                        <button class="btn btn-round btn-primary" data-toggle="modal" data-target="#loginModal">
                            Compartilhar
                        </button>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6" style="align-self: center">
                        <!--Carousel Wrapper-->
                        <div id="carousel-thumb" class="carousel carousel-fade carousel-thumbnails"
                             data-ride="carousel">
                            <!--Slides-->
                            <div class="carousel-inner" role="listbox">
                                @php $i = 0 @endphp
                                @forelse($realty->files as $photo)
                                    <div class="carousel-item {{ $i == 0 ? 'active' : '' }}">
                                        <img class="d-block w-100"
                                             src="{{ $photo->url }}"
                                             alt="{{ $realty->title }}">
                                    </div>

                                    @php $i++ @endphp

                                @empty
                                    <div class="carousel-item {{ $i == 0 ? 'active' : '' }}">
                                        <img class="d-block w-100"
                                             src="https://vaganzi.s3-sa-east-1.amazonaws.com/sem-foto.jpg"
                                             alt="{{ $realty->title }}">
                                    </div>
                                @endforelse
                            </div>
                            <!--/.Slides-->

                            <!--Controls-->
                            <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            <!--/.Controls-->

                            <ol class="carousel-indicators">
                                @php $i = 0 @endphp
                                @forelse($realty->files as $photo)
                                    <li data-target="#carousel-thumb"
                                        data-slide-to="{{ $i }}"
                                        class="{{ $i == 0 ? 'active' : '' }}">
                                        <img class="d-block w-100"
                                             src="{{ $photo->url }}"
                                             class="img-fluid">
                                    </li>

                                    @php $i++ @endphp

                                @empty

                                    <li data-target="#carousel-thumb"
                                        data-slide-to="{{ $i }}"
                                        class="{{ $i == 0 ? 'active' : '' }}">
                                        <img class="d-block w-100"
                                             src="https://vaganzi.s3-sa-east-1.amazonaws.com/sem-foto.jpg"
                                             alt="{{ $realty->title }}"
                                             class="img-fluid">
                                    </li>

                                @endforelse
                            </ol>
                        </div>
                        <!--/.Carousel Wrapper-->

                    </div>

                    {{-- DESCRIÇÃO --}}
                    <div class="col-sm-12 col-md-6">
                        <h1 class="title web">{{ $realty->title }}</h1>
                        <h1 class="title mobile" style="font-size: 1.5rem">{{ $realty->title }}</h1>

                        <span class="badge badge-default">{{ $realty->ad->vacancyType->name }}</span>

                        <h3 class="main-price">R$ {{ $realty->ad->price }}</h3>

                        <div class="ad-details">
                            <div class="ad-description">
                                <p>@php echo nl2br($realty->description) @endphp</p>
                            </div>

                            <div class="ad-address">
                                <h5 class="title realty-details-title">Localização</h5>
                                <p>{{ "{$realty->address->address}, {$realty->address->number} - {$realty->address->neighborhood}" }}</p>
                            </div>

                            <div class="realty-details">
                                <div class="realty-details-details">
                                    <div class="font-dd">
                                        <dt>Nº de quarto</dt>
                                        <dd>{{ $realty->room->qtde }}</dd>
                                    </div>

                                    <div class="font-dd">
                                        <dt>Garagem Moto</dt>
                                        <dd>{{ isset($realty->garageType[0]) ? $realty->garageType[0]->pivot->qtde : '' }}</dd>
                                    </div>

                                    <div class="font-dd">
                                        <dt>Garagem Carro</dt>
                                        <dd>{{ isset($realty->garageType[1]) ? $realty->garageType[1]->pivot->qtde : '' }}</dd>
                                    </div>
                                </div>

                                <div>
                                    <h5 class="title realty-details-title">Anunciante</h5>

                                    <div class="realty-details-contact">
                                        <div class="row" style="margin-top: 25px">
                                            <div class="col-sm-12 col-md-6">
                                                <b>Nome</b>
                                                <div>
                                                    <a href="{{ url("/perfil/{$realty->profile[0]->slug}") }}">{{ $realty->profile[0]->name . ' ' . $realty->profile[0]->last_name }}</a>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <b>Contato</b>
                                                <div>
                                                    <a href="{{ $realty->wapp }}"
                                                       id="whatsapp-contact"
                                                       rel="nofollow"
                                                       class=""
                                                       target="_blank"><img
                                                                src="{{ asset('img/icons8-whatsapp-24.png') }}"/></a>
                                                    <a href="{{ $realty->wapp }}"
                                                       id="whatsapp-contact"
                                                       rel="nofollow"
                                                       class="mobile-phone-with-ddd"
                                                       target="_blank">{{ $realty->profile[0]->contact->mobile_phone }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="title">Localização no Mapa</h3>
                    </div>
                    <div class="col-sm-12">
                        <div id="mapId" style="height: 400px; margin-bottom: 25px;">
                            @if (empty($realty->address->latitude))
                                {{ 'Endereço não informado' }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="loginModal" tabindex="-1" role="">
        <div class="modal-dialog modal-login" role="document">
            <div class="modal-content">
                <div class="card card-signup card-plain">
                    <div class="modal-header">
                        <div class="card-header card-header-primary text-center">
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">
                                <i class="material-icons">clear</i>
                            </button>

                            <h4 class="card-title">Compartilhe este anúncio com seus amigos</h4>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">
                            <div class="col-sm-12">
                                <a type="button"
                                   class="btn btn-default"
                                   href="https://www.facebook.com/sharer/sharer.php?u={{ rawurlencode(Request::url()) }}"
                                   target="_blank">Facebook</a>
                            </div>
                            <div class="col-sm-12">
                                <a type="button"
                                   class="btn btn-default"
                                   href="https://wa.me/send?text={{ urlencode('Dê uma olhada nesse anuncio no Vaganzi! ' . Request::url()) }}"
                                   target="_blank">Whatsapp</a>
                            </div>
{{--                            <div class="col-sm-12">--}}
{{--                                <button type="button"--}}
{{--										id="btn-copiar-link"--}}
{{--										class="btn btn-default">Copiar Link</button>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
                @if (!empty($realty->address->latitude))
        var map = L.map('mapId').setView([{{ $realty->address->latitude }}, {{ $realty->address->longitude }}], 17);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        L.marker([{{ $realty->address->latitude }}, {{ $realty->address->longitude }}])
            .bindPopup('{{ $realty->title }}')
            .addTo(map);

        document.documentElement.setAttribute("lang", "en");
        document.documentElement.removeAttribute("class");

        axe.run(function (err, results) {
            console.log(results.violations);
        });
        @endif
    </script>
@stop