<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="name">* Nome</label>
            <input type="text"
                   class="form-control"
                   name="name"
                   id="name"
                   maxlength="255"
                   value="{{ isset($product->name) ? $product->name : '' }}"
                   required/>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <label for="description">* Descrição</label>
            <textarea class="form-control"
                      id="description"
                      name="description"
                      rows="3"
                      maxlength="255"
                      required>{{ isset($product->description) ? $product->description : '' }}</textarea>
        </div>
    </div>

    <div class="col-sm-12 col-md-3">
        <div class="form-group">
            <label for="purchase_price">* Preço de Compra</label>
            <input type="text"
                   class="form-control money"
                   name="purchase_price"
                   id="purchase_price"
                   value="{{ isset($product->purchase_price) ? $product->purchase_price : '' }}"
                   required/>
        </div>
    </div>

    <div class="col-sm-12 col-md-3">
        <div class="form-group">
            <label for="sale_price">* Preço de Venda</label>
            <input type="text"
                   class="form-control money"
                   name="sale_price"
                   id="sale_price"
                   value="{{ isset($product->sale_price) ? $product->sale_price : '' }}"
                   required/>
        </div>
    </div>

    <div class="col-sm-12 col-md-3">
        <div class="form-group">
            <label for="category_id">* Categoria</label>
            <select id="category_id" name="category_id" class="form-control" required>
                <option disabled>Escolha a Categoria</option>
                @foreach($categories as $category)
                    <option {{ isset($product) && $category->id == $product->category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-sm-12 col-md-3">
        <div class="form-group">
            <label for="stock">* Estoque</label>
            <input type="text"
                   class="form-control integer"
                   name="stock"
                   id="stock"
                   value="{{ isset($product->stock) ? $product->stock : '' }}"
                   required/>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group">
            <div class="custom-file">
                <input type="file"
                       class="custom-file-input"
                       id="file"
                       name="file"
                       value="{{ $product->file->real_name ?? '' }}"
                       accept="image/png, image/jpeg"
                       aria-describedby="validationServer03Feedback">
                <label class="custom-file-label"
                       for="file">{{ $product->file->real_name ?? '* Escolha uma foto' }}</label>
            </div>
            <div id="validationServer03Feedback" class="invalid-feedback">
                Selecione uma foto para o produto
            </div>
        </div>
    </div>
</div>

<script>
    var file = document.getElementById('file')
    var validation = document.getElementById('validationServer03Feedback')
    var form = document.getElementById('form')

    file.addEventListener('change', (event) => {
        if (file.value != '') {
            var textLabel = document.querySelector('label[for="file"]')
            textLabel.textContent = file.value.split(/(\\|\/)/g).pop()
        }
    })

    function validationProduct() {
        if (file.files.length == 0 && file.defaultValue == '') {
            file.required = true
            validation.style.display  = 'block'

            return false
        }

        file.required = false
        validation.style.display = 'none'

        return true
    }
</script>
