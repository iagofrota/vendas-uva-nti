@extends('layouts.app')
@section('content')
    <div>
        <div class="container container-margin-top">
            <div class="form-row">
                <div class="col-sm-12">
                    @include('partials.alerts')
                </div>
            </div>

            <form action="{{ route('products.index') }}"
                  method="get">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Pesquisar</h3>

                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <label for="name">Nome</label>
                                            <input type="text"
                                                   class="form-control"
                                                   name="name"
                                                   id="name"
                                                   maxlength="255"
                                                   value="{{ isset($_GET['name']) ? $_GET['name'] : '' }}"/>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <label for="category_id">Categoria</label>
                                            <select id="category_id" name="category_id" class="form-control">
                                                <option selected value="">Escolha a Categoria</option>
                                                @foreach($categories as $category)
                                                    <option
                                                        {{ isset($_GET['category_id']) && $category->id == $_GET['category_id'] ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <button class="btn btn-secondary"
                                                type="submit">
                                            Pesquisar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Produtos</h3>

                            <a href="{{ route('products.create') }}" role="button" class="btn btn-primary">
                                Adicionar Produto
                            </a>

                            <div>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Preço de Compra</th>
                                            <th>Preço de Venda</th>
                                            <th>Categoria</th>
                                            <th>Estoque</th>
                                            <th class="text-sm-center">Ações</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($products as $product)
                                            <tr>
                                                <td>{{ $product->name }}</td>
                                                <td>R$ {{ $product->purchase_price }}</td>
                                                <td>R$ {{ $product->sale_price }}</td>
                                                <td>{{ $product->category->name }}</td>
                                                <td>{{ $product->stock }}</td>
                                                <td class="text-sm-center">
                                                    <a href="{{ route('products.edit', $product) }}"
                                                       role="button"
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       title="Atualizar Produto"
                                                       class="btn btn-secondary">
                                                        Alterar
                                                    </a>

                                                    <a data-toggle="modal"
                                                       data-placement="top"
                                                       title="Excluir imóvel"
                                                       data-url="{{ route('products.destroy', $product) }}"
                                                       data-id="{{ $product->id }}"
                                                       data-target="#custom-width-modal"
                                                       data-csrf="{{ csrf_token() }}"
                                                       class="btn btn-danger remove-record"
                                                       role="button">
                                                        Excluir
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(!empty($products))
                <div class="row">
                    <div class="col-sm-12">
                        {{ $products->links() }}
                    </div>
                </div>
            @endif
        </div>
    </div>

    @include('partials.deleteModel')
@stop
