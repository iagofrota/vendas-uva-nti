<div id="custom-width-modal"
     class="modal fade"
     tabindex="-1"
     role="dialog"
     aria-labelledby="custom-width-modalLabel"
     aria-hidden="true" style="display: none;">

	<form action=""
	      method="POST"
	      id="modal-confirmation-destroy"
	      class="remove-record-model">
		@csrf
		{{ method_field('DELETE') }}

		<div class="modal-dialog" style="width:55%;">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Deseja excluir este item?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Esta ação irá excluir este item.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default remove-data-from-delete-form" data-dismiss="modal" style="margin-right: 5px;">
						Não
					</button>
					<button type="submit" class="btn btn-danger">Sim, eu quero excluir</button>
				</div>
			</div>
		</div>
	</form>
</div>
