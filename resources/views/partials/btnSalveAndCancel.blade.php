<div class="row" style="margin-top: 20px">
    <div class="col-sm-12">
        <button type="submit" id="btn-save" class="btn btn-primary">
            Salvar
        </button>
        <a href="{{ $route }}" role="button" class="btn btn-secondary">
            Cancelar
        </a>
    </div>
</div>
