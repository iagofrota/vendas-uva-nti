@if ($errors->any())
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
            <span aria-hidden="true">×</span>
        </button>
        <strong>Erro!</strong> Por favor, verifique os erros abaixo:
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
            <span aria-hidden="true">×</span>
        </button>
        <strong>Sucesso!</strong> <?php echo $message; ?>
    </div>
	<?php Session::forget('success');?>
@endif
@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
            <span aria-hidden="true">×</span>
        </button>
        <strong>Erro!</strong> <?php echo $message; ?>
    </div>
	<?php Session::forget('error');?>
@endif
@if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
            <span aria-hidden="true">×</span>
        </button>
        <strong>Atenção!</strong> <?php echo $message; ?>
    </div>
	<?php Session::forget('warning');?>
@endif
@if ($message = Session::get('info'))
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Fechar">
            <span aria-hidden="true">×</span>
        </button>
        <strong>Informação!</strong> <?php echo $message; ?>
    </div>
	<?php Session::forget('info');?>
@endif