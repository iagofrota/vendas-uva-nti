<!DOCTYPE html>
<html>
<head>
    <title>Vendas NTI</title>
</head>
    <body>
        <h2>Olá {{ $sale->client->name }}</h2>
        <p>Segue os dados da sua compra</p>
        <p><b>Data da Venda:</b> {{ $sale->created_at }}</p>
        <p><b>Total sem Desconto:</b> {{ $sale->total_without_discount }}</p>
        <p><b>Total com Desconto:</b> {{ $sale->total_with_discount }}</p>
        <p><b>Link para acesso:</b> <a href="{{ env('LINK_CONSULTA', 'http://consultanti.test/') . '?cpf=' . $sale->client->cpf }}" target="_blank">{{ env('LINK_CONSULTA', 'http://consultanti.test/') . '?cpf=' . $sale->client->cpf }}</a></p>

        <div style="margin-top: 12px">
            <table>
                <thead>
                <tr>
                    <th>Produto</th>
                    <th>Quantidade</th>
                    <th>Preço</th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($sale->products as $product)
                        <tr>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->pivot->amount }}</td>
                            <td>R$ {{ $product->sale_price }}</td>
                            <td>R$ {{ \App\Utils::currencyToDisplay(floatval($product->sale_price) * floatval($product->pivot->amount)) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </body>
</html>
