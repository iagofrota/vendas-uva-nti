@extends('layouts.app')
@section('content')
    <section class="section">
        <form action="{{ route('sales.update', $sale->id) }}"
              method="post"
              id="form"
              enctype="multipart/form-data">
            @method('PUT')
            @csrf

            <div class="container container-margin-top">
                <div class="row">
                    <div class="col-sm-12">
                        @include('partials.alerts')
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title"> Atualizar Venda</h3>
                                @include('sales.form')
                            </div>
                        </div>
                    </div>
                </div>

                @component('partials.btnSalveAndCancel', [
                    'route' => route('sales.index')
                ])
                @endcomponent
            </div>
        </form>
    </section>
@stop
