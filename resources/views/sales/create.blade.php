@extends('layouts.app')
@section('content')

    <section class="section">
        <form action="{{ route('sales.store') }}"
              method="post"
              id="form"
              enctype="multipart/form-data">
            @csrf

            <div class="container container-margin-top">
                <div class="row">
                    <div class="col-sm-12">
                        @include('partials.alerts')
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Adicionar Venda</h3>
                                @include('sales.form')
                            </div>
                        </div>
                    </div>
                </div>

                @component('partials.btnSalveAndCancel', [
                   'route' => route('sales.index')
               ])
                @endcomponent
            </div>
        </form>
    </section>
@stop
