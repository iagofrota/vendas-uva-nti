@extends('layouts.app')
@section('content')
    <section class="section">
        <div class="container container-margin-top">
            <div class="form-row">
                <div class="col-sm-12">
                    @include('partials.alerts')
                </div>
            </div>

            <form action="{{ route('sales.index') }}"
                  method="get">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Pesquisar</h3>

                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <label for="name">Nome do Cliente</label>
                                            <input type="text"
                                                   class="form-control"
                                                   name="name"
                                                   id="name"
                                                   maxlength="255"
                                                   value="{{ isset($_GET['name']) ? $_GET['name'] : '' }}" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <button class="btn btn-secondary"
                                                type="submit">
                                            Pesquisar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">Vendas</h3>

                            <a href="{{ route('sales.create') }}" role="button" class="btn btn-primary">
                                Adicionar Venda
                            </a>

                            <div>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Cliente</th>
                                            <th>Total sem Desconto</th>
                                            <th>Total com Desconto</th>
                                            <th>Data</th>
                                            <th class="text-sm-center">Ações</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($sales as $sale)
                                                <tr>
                                                    <td>{{ $sale->client->name }}</td>
                                                    <td>R$ {{ $sale->total_without_discount }}</td>
                                                    <td>R$ {{ $sale->total_with_discount }}</td>
                                                    <td>{{ $sale->created_at }}</td>
                                                    <td class="text-sm-center">
                                                        <a href="{{ route('sales.edit', $sale) }}"
                                                           role="button"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           title="Atualizar Venda"
                                                           class="btn btn-secondary">
                                                            Alterar
                                                        </a>

                                                        <a data-toggle="modal"
                                                           data-placement="top"
                                                           title="Excluir imóvel"
                                                           data-url="{{ route('sales.destroy', $sale) }}"
                                                           data-id="{{ $sale->id }}"
                                                           data-target="#custom-width-modal"
                                                           data-csrf="{{ csrf_token() }}"
                                                           class="btn btn-danger remove-record"
                                                           role="button">
                                                            Excluir
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(!empty($sales))
                        <div class="row">
                            <div class="col-sm-12">
                                {{ $sales->links() }}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>

    @include('partials.deleteModel')
@stop
