<div class="row">
    <div class="col-sm-12">
        <h5>Cliente</h5>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-2">
        <div class="form-group">
            <label for="cpf">* CPF</label>
            <input type="text"
                   class="form-control cpf"
                   name="cpf"
                   id="cpf"
                   value="{{ isset($sale->client->cpf) ? $sale->client->cpf : '' }}"
                   required/>
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="name">* Nome</label>
            <input type="text"
                   class="form-control"
                   name="name"
                   id="name"
                   maxlength="255"
                   value="{{ isset($sale->client->name) ? $sale->client->name : '' }}"
                   required/>
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="email">* E-mail</label>
            <input type="email"
                   class="form-control"
                   name="email"
                   id="email"
                   maxlength="255"
                   value="{{ isset($sale->client->email) ? $sale->client->email : '' }}"
                   required/>
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="form-group">
            <label for="telephone">* Celular/Telefone</label>
            <input type="telephone"
                   class="form-control mobile-phone"
                   name="telephone"
                   id="telephone"
                   value="{{ isset($sale->client->telephone) ? $sale->client->telephone : '' }}"
                   required/>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h5>Venda</h5>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-2">
        <div class="form-group">
            <label for="discount">* Desconto (%)</label>
            <input type="discount"
                   class="form-control money"
                   name="discount"
                   id="discount"
                   value="{{ isset($sale->discount) ? $sale->discount : '' }}"
                   required/>
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="form-group">
            <label for="total">Total</label>
            <input type="total"
                   class="form-control money"
                   name="total"
                   readonly
                   id="total"
                   value="{{ isset($sale->total_without_discount) ? $sale->total_without_discount : '' }}"
                   required/>
        </div>
    </div>
    <div class="col-sm-12 col-md-2">
        <div class="form-group">
            <label for="total_with_discount">Total com Desconto</label>
            <input type="total_with_discount"
                   class="form-control money"
                   id="total_with_discount"
                   disabled
                   value="{{ isset($sale->total_with_discount) ? $sale->total_with_discount : '' }}"
                   required/>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-6">
        <div class="form-group">
            <label for="product_id">Produto</label>
            <select id="product_id" class="form-control">
                <option selected value="">Escolha o Produto</option>
                @foreach($products as $product)
                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="quantity">Quantidade</label>
            <input type="quantity"
                   class="form-control integer"
                   id="quantity"
                   value="" />
        </div>
    </div>

    <div class="col-sm-12 col-md-2" style="align-self: center; margin-top: 13px;">
        <button type="button"
                id="btn_add_product"
                class="btn btn-secondary">Adicionar
        </button>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <table class="table table-striped table_product">
            <thead>
            <tr>
                <th></th>
                <th>Produto</th>
                <th>Quantidade</th>
                <th>Preço</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody class="tbody_product">

            </tbody>
        </table>
    </div>
</div>

<input type="hidden" id="json_products" value="{{ $products->toJson() }}"/>
<input type="hidden" id="products" name="products" value="{{ isset($sale->products) ? json_encode($sale->products) : '' }}"/>
