<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(3),
        'description' => $faker->paragraph,
        'sale_price' => $faker->numberBetween(100, 500),
        'purchase_price' => $faker->numberBetween(50, 100),
        'category_id' => function () {
            return factory(\App\Category::class)->create()->id;
        },
        'file_id' => function () {
            return factory(\App\Files::class)->create()->id;
        },
        'stock' => $faker->numberBetween(1, 10),
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
