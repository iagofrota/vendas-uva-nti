<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Files;
use Faker\Generator as Faker;

$factory->define(Files::class, function (Faker $faker) {
    $fake_name = $faker->image(storage_path('app/public'), 800, 640, null, false);

    return [
        'real_name' => $faker->word . '.jpg',
        'fake_name' => $fake_name,
        'extension' => 'jpg',
        'size' => $faker->numberBetween(0, 2048),
        'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
        'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
    ];
});
