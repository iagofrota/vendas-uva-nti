<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/client', function (Request $request) {
    $sale = \App\Sale::with(['client', 'products'])
        ->whereHas('client', function ($query) use ($request) {
            $query->where('clients.cpf', \App\Utils::onlyNumbers($request->get('cpf')));
        })->get()->toJson();

    return $sale;
});

Route::get('/client', function (Request $request) {
    if (!empty($request->get('cpf'))) {
        $sale = \App\Sale::with(['client', 'products'])
            ->whereHas('client', function ($query) use ($request) {
                $query->where('clients.cpf', \App\Utils::onlyNumbers($request->get('cpf')));
            })->get()->toJson();

        return $sale;
    }
});
