<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    // Produtos
    Route::get('produtos', 'ProductController@index')->name('products.index');
    Route::get('produto/criar', 'ProductController@create')->name('products.create');
    Route::post('produto/salvar', 'ProductController@store')->name('products.store');
    Route::get('produto/alterar/{product}', 'ProductController@edit')->name('products.edit');
    Route::put('produto/alterando/{product}', 'ProductController@update')->name('products.update');
    Route::delete('produto/excluir/{product}', 'ProductController@destroy')->name('products.destroy');

    // Vendas
    Route::get('vendas', 'SaleController@index')->name('sales.index');
    Route::get('venda/criar', 'SaleController@create')->name('sales.create');
    Route::post('venda/salvar', 'SaleController@store')->name('sales.store');
    Route::get('venda/alterar/{sale}', 'SaleController@edit')->name('sales.edit');
    Route::put('venda/alterando/{sale}', 'SaleController@update')->name('sales.update');
    Route::delete('venda/excluir/{sale}', 'SaleController@destroy')->name('sales.destroy');
});
